from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json
import requests

# get photo function takes city and state as parameters and uses
# Pexels API to get a photo of that location
def get_photo(city, state):
    # establish the header for metadata includes the API key
    headers = {"Authorization": PEXELS_API_KEY}
    # establish the parameters that will go into the URL to get a photo
    # query is required it will query the city and state
    # per page is the number of images tht will be provided per page
    params = {"query": city + " " + state, "per_page": 1}
    # the base URL for the API
    url = "https://api.pexels.com/v1/search"
    # response uses requests.get to get response from the URL with
    # the headers and the parameters
    response = requests.get(url, headers=headers, params=params)
    # Content that will be returned will be python due to json.loads
    content = json.loads(response.content)
    # Try and except block incase there is no picture URL it will return None
    try:
        # The content that is returned is a dictionsary with "photos" as a key
        # It's value is a list, the first index item of the list is a dictionary
        # "src" is also a key in a dictionary it's value is another dictionary
        #  and "original" is one the keys in that dictionary
        # so it's value is what is returned
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except:
        return {"picture_url": None}


def get_weather_data(city, state):
    us_iso_code = "ISO 3166-2:US"
    geo_url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},{us_iso_code}&limit=1&appid={OPEN_WEATHER_API_KEY}"
    response_geo = requests.get(geo_url)
    geocode = json.loads(response_geo.content)
    lat = geocode[0]["lat"]
    lon = geocode[0]["lon"]

    weather_url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}"
    weather_response = requests.get(weather_url)
    weather_data = json.loads(weather_response.content)
    temperature = weather_data["main"]["temp"]
    description = weather_data["weather"][0]["description"]
    try:
        return {
            "temperature": temperature,
            "description": description,
        }
    except:
        return {"weather": None}
